package com.example.miguelparra.sharedpreferencesagenda


import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    internal lateinit var et_nombre: EditText
    internal lateinit var et_datos: EditText
    internal val TAG = MainActivity::class.java.simpleName// L29almacenar el nombre en la clase en la que estiy
    internal lateinit var boton_guardar: Button
    internal lateinit var boton_buscar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        et_nombre = findViewById(R.id.txt_nombre)
        et_datos = findViewById(R.id.txt_datos)
        boton_guardar = findViewById<Button>(R.id.id_botonGuardar)
        boton_buscar = findViewById<Button>(R.id.id_botonBuscar)


        boton_guardar.setOnClickListener{ guardar() }
        boton_buscar.setOnClickListener{ buscar() }

    }

    fun guardar() {
        val nombre = et_nombre.text.toString() //almacenar temporalmente el nombre del contacto
        val datos = et_datos.text.toString()

        val preferencias: SharedPreferences = this.getSharedPreferences("agenda", 0)// 0 en lugar de poner Context.MODE_PRIVATE
        val editor_preferences = preferencias.edit() //obeto para pader editar nuestro archivo
        editor_preferences.putString(nombre, datos) // colocando los valores, el primer parametro es la referencia del nombre a guardar  y el segundo los datos
        editor_preferences.commit()// indicar que realmente se quiere que se guarde estos valores

        Toast.makeText(this, "El contacto ha sido guardado", Toast.LENGTH_SHORT).show()// darlela confirmacion que en efecto se ha almacenado de manera correcta
        //Log.d(TAG, "contacto guardado")
        et_nombre.setText("");
        et_datos.setText("");


    }

    fun buscar() {
        val nombre = et_nombre.text.toString() //recuperar el nombre escrito

        val preferencias: SharedPreferences = this.getSharedPreferences("agenda", 0) //dentro de este objeto vamos almacenar en primer lugar el nombre del archivo
        val datos = preferencias.getString(nombre,"")// dentro de esta variable vamoa almacenar los datos que posteriormente se van a mostrar del contacto que se esta buscando.
        // se pide primero la referencia y despues el valor que se quiere mostrarque en este caso son los datos por ende poinemos un par de comillas

        if(datos.length == 0) {
            Toast.makeText(this,"No se encontro nungun registro", Toast.LENGTH_SHORT ).show()
            //Log.d(TAG, "No se encontro ningun registro")
        }else{
            et_datos.setText(datos)
        }
        }

}
